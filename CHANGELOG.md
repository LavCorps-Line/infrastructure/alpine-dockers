* Alpine 3.17
  * BREAKING CHANGE: Dockerfile automatically drops root privileges (Access to
apk is still allowed using sudo).
* Alpine 3.18
  * Add LavCorps Repository via tag `@lavcorps`
* Alpine 3.19
  * BREAKING CHANGE: Replace sudo with doas+doas-sudo-shim.
    * Access to apk without authentication is still allowed.
* Alpine 3.20
  * Added Nushell to standard dockerfile