let Target = ./Target.dhall

let Targets =
      [   { name = "edgetesting"
          , commands = [ "curl -V", "git -v" ] : List Text
          , platforms = [ "amd64", "arm64" ] : List Text
          , stable = False
          }
        : Target
      ,   { name = "edge"
          , commands = [ "curl -V", "git -v" ] : List Text
          , platforms = [ "amd64", "arm64" ] : List Text
          , stable = False
          }
        : Target
      ,   { name = "3.18"
          , commands =
                [ "luajit -v"
                , "zsh --version"
                , "python3 --version"
                , "pip --version"
                ]
              : List Text
          , platforms = [ "amd64" ] : List Text
          , stable = True
          }
        : Target
      ,   { name = "3.19"
          , commands =
                [ "luajit -v"
                , "zsh --version"
                , "python3 --version"
                , "pip --version"
                ]
              : List Text
          , platforms = [ "amd64" ] : List Text
          , stable = True
          }
        : Target
      ,   { name = "3.20"
          , commands =
                [ "luajit -v"
                , "zsh --version"
                , "python3 --version"
                , "pip --version"
                , "git -v"
                ]
              : List Text
          , platforms = [ "amd64", "arm64" ] : List Text
          , stable = True
          }
        : Target
      ,   { name = "3.21"
          , commands =
                [ "curl -V"
                , "git -v"
                ]
              : List Text
          , platforms = [ "amd64", "arm64" ] : List Text
          , stable = True
          }
        : Target
      ]

in  Targets
