let GitLab = ./GitLab.dhall

let Prelude = ./Prelude.dhall

let JobType = ./JobType.dhall

let Target = ./Target.dhall

let BuildCommands = ./BuildCommands.dhall

let BuilderURL = ./BuilderURL.dhall

let RepoURL = ./RepoURL.dhall

let DefaultTestCommands = ./DefaultTestCommands.dhall

let MkJob
    : forall (type : Natural) ->
      forall (build : Bool) ->
      forall (target : Target) ->
        GitLab.Job.Type
    = \(type : Natural) ->
      \(build : Bool) ->
      \(target : Target) ->
        GitLab.Job::{
        , stage = Some (if build then "build" else "test")
        , image = Some
            ( if    build
              then  GitLab.Image::{
                    , name = BuilderURL
                    , entrypoint = Some [ "" ]
                    }
              else  if Prelude.Natural.equal type JobType.latest
              then  GitLab.Image::{
                    , name =
                        "${RepoURL}/${target.name}:commit-\$CI_COMMIT_SHORT_SHA"
                    , entrypoint = Some [ "" ]
                    }
              else  if Prelude.Natural.equal type JobType.stable
              then  GitLab.Image::{
                    , name =
                        "${RepoURL}/${target.name}:release-\${OG_COMMIT_TAG}"
                    , entrypoint = Some [ "" ]
                    }
              else  GitLab.Image::{
                    , name = "${RepoURL}/${target.name}:stable"
                    , entrypoint = Some [ "" ]
                    }
            )
        , script =
            if    build
            then  if    Prelude.Natural.equal type JobType.latest
                  then  BuildCommands.latest target
                  else  if Prelude.Natural.equal type JobType.stable
                  then  BuildCommands.stable target
                  else  BuildCommands.scheduled target
            else  DefaultTestCommands # target.commands
        , variables = toMap { buildVer = target.name }
        , needs =
            if    build
            then  [] : List Text
            else  [ Prelude.Text.concatSep
                      " "
                      [ "build"
                      , if    Prelude.Natural.equal type JobType.latest
                        then  "latest"
                        else  if Prelude.Natural.equal type JobType.stable
                        then  "stable"
                        else  "scheduled"
                      , target.name
                      ]
                  ]
        , rules = Some
          [ GitLab.Rule::{
            , `if` = Some
                ( if    Prelude.Natural.equal type JobType.latest
                  then  "\$OG_COMMIT_TAG == \"\" && \$OG_PIPELINE_SOURCE != \"schedule\""
                  else  if Prelude.Natural.equal type JobType.stable
                  then  "\$OG_COMMIT_TAG != \"\" && \$OG_PIPELINE_SOURCE != \"schedule\""
                  else  "\$OG_PIPELINE_SOURCE == \"schedule\""
                )
            }
          ]
        , retry = Some GitLab.Retry::{
          , max = Some GitLab.Max.Type.`2`
          , when =
            [ GitLab.FailCondition.Type.RunnerSystemFailure
            , GitLab.FailCondition.Type.StuckOrTimeoutFailure
            , GitLab.FailCondition.Type.ApiFailure
            , GitLab.FailCondition.Type.ScriptFailure
            ]
          }
        }

in  MkJob
