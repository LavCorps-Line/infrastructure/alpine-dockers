-- /.gitlab-ci.dhall
let GitLab = ./.dhall/GitLab.dhall

let Prelude = ./.dhall/Prelude.dhall

let Jobs = ./.dhall/Jobs.dhall

in  Prelude.JSON.renderYAML
      ( GitLab.Top.toJSON
          GitLab.Top::{ jobs = Jobs, stages = Some [ "build", "test" ] }
      )
